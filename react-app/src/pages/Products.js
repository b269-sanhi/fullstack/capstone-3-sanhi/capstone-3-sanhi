import {useState, useEffect, useContext} from 'react';
import ProductCard from '../components/ProductCard'

export default function GetAllProducts() {	

	const [products, setProducts] = useState([])
	console.log (products)
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				
				return(

					<ProductCard key={product._id} product={product} />
				)

			}))

		})
	}, [])

	return (
		<>
		{products}
		</>
	)
}