import {useState, useEffect, useContext} from 'react';
import ProductCard from '../components/ProductCard'

export default function GetAllProducts() {	

	const [products, setProducts] = useState([])
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(products => {
				return(
					<ProductCard key={products._id} products={products} />
				)
			}))
		})
	}, [])

	return (
		<>
		{products}
		</>
	)
}