import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import ProductCard from '../components/ProductCard'

export default function UpdateProducts() {  

  const [products, setProducts] = useState([])
   const [name, setName] = useState([]);
  const [description, setDescription] = useState([]);
  const [price, setPrice] = useState([]);
  const [stock, setStock] = useState([]);

   const updateProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/:productId`, {
      method: 'PUT',
      headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
      body: JSON.stringify
      ({ 
      name : name,
      description : description,
      price : price,
      stock : stock
       })
        .then(res => res.json())
    .then(data => {
      console.log(data);
      setProducts(data.map(products => {
        return(
          <ProductCard key={products._id} products={products} />
        )
      }))
    })
  }, [])

  return (
    <>
    {products}
    </>
    )}
}