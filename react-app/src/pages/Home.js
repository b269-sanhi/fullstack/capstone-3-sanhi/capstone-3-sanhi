import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

    const data = {
        title: "Save dogs",
        content: "Become a fur parent",
        destination: "/products",
        label: "Order now!"
    }


    return (
        <>
        <Banner data={data} />
        <Highlights />
        
        </>
    )
}
