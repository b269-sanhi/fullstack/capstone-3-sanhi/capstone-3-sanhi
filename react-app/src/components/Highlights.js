
import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';

export default function ControlledCarousel() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://place-puppy.com/250x250"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>We got what you want</h3>
          <p>Bring happiness to your homes</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://place-puppy.com/250x250"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Choose what's best for you</h3>
          <p>We got you! Free delivery nationwide</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://place-puppy.com/250x250"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Order now!</h3>
          <p>
            Hurry!
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

